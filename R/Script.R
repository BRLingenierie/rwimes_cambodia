# rm(list=ls(all=TRUE))
# library(rwimes)
# rwimes::wimesInitialization("https://wimes-cambodia-undp.brl.fr/api", "f7fc0ed3-a340-4f6c-8ab9-b5693a9f347f")
# 
# source("global_parameters.R")
# source("subfunctions.R")
# source("wimes_functions.R")
# source("modelisation_gr.r")
# bv <- "BV_Chinit;BV_Sen;BV_PeaKhl;BV_A600101;BV_BakTra;BV_KomKde;BV_A550102;BV_Bor;BV_PraKeo"
# ff <- "FF_A620101;FF_A610101;FF_PeaKhl;FF_A600101;FF_BakTra;FF_KomKde;FF_A550102;FF_Bor;FF_PraKea"
# rf <- "RF_A620101;RF_A610101;RF_PeaKhl;RF_A600101;RF_BakTra;RF_KomKde;RF_A550102;RF_Bor;RF_PraKea"
# S_GR4 <-"GRS_A620101;GRS_A610101;GRS_PeaKhl;GRS_A600101;GRS_BakTra;GRS_KomKde;GRS_A550102;GRS_Bor;GRS_PraKea"
# R_RG4 <- "GRR_A620101;GRR_A610101;GRR_PeaKhl;GRR_A600101;GRR_BakTra;GRR_KomKde;GRR_A550102;GRR_Bor;GRR_PraKea"
# fo <- "FO_A620101;FO_A610101;FO_PeaKhl;FO_A600101;FO_BakTra;FO_KomKde;FO_A550102;FO_Bor;FO_PraKea"
# ro <- "RO_A620101;RO_A610101;RO_PeaKhl;RO_A600101;RO_BakTra;RO_KomKde;RO_A550102;RO_Bor;RO_PraKea"
# AA <- "100;113.77;943.88;27.03;1045.12;404.23;27.94;1900.74;959.32"
# ECH <- "-50.02;-65.94;-5.95;-65.14;0.07;-29.24;-9.12;-6.06;-11.28"
# BB <- "1032.77;1123.36;73.7;680.49;144.32;179.34;720.54;239.85;338.05"
# CC <- "3.71;6.29;5.4;4.24;2.27;4.31;2.66;1.14;3.29"
# area <- "4438;14295;3749;2305;4908;2076;3092;1119;520"
# gridSiteCode_rf <- "GFS_GRID_FC_P_3H"
# gridSiteCode_ro <- "GPM_GRID_P_1D"
# 
# airgr_wimes_cambodia_all(bv, ff, rf, fo, ro, S_GR4, R_RG4, AA, ECH, BB, CC, area, gridSiteCode_rf, gridSiteCode_ro, FALSE)
# 


#airgr_wimes_cambodia("BV_BakTra", "FF_BakTra", "RF_BakTra", "GFS_GRID_FC_P_3H", "GRS_BakTra", "GRR_BakTra", 1045.12, 0.07, 144.32, 2.27, 4908, "GPM_GRID_P_1D", "FO_BakTra", "RO_BakTra", silent = FALSE)