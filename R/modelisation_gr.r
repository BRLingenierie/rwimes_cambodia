# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation

#' Run the GR4J model for all basins
#'
#' @export
#' @param bv_codes - list of the codes of the polygon sites that represent the watersheeds of the prevision points
#' @param ff_codes - list of the codes of the timeseries which will recieve the flow forecast
#' @param rf_codes - list of the codes of the timeseries which will recieve the precipitation forecast
#' @param fo_codes - list of the codes of the timeseries which will recieve the observed flows
#' @param ro_codes - list of the codes of the timeseries which will recieve the observed rainfalls
#' @param S_GR4_codes - list of the codes of internal timeseries for real-time calculation
#' @param R_GR4_codes - list of the codes of internal timeseries for real-time calculation
#' @param AA_values - list of parameters of GR4J model x1
#' @param ECH_values - list of parameters of GR4J model x2
#' @param BB_values - list of parameters of GR4J model x3
#' @param CC_values - list of parameters of GR4J model x4
#' @param AREA_values - list of  the basin areas in square kilometers
#' @param gridSiteCode_rf - code of the timeseries which contains the rainfall forecast as grid
#' @param gridSiteCode_ro - code of the timeseries which contains the observed rainfall as grid
#' @param save - TRUE to write results in database, FALSE to write resulst in the window
#'
airgr_wimes_cambodia_all <- function(bv_codes, ff_codes, rf_codes, fo_codes, ro_codes, S_GR4_codes, R_GR4_codes, AA_values, ECH_values, BB_values, CC_values, AREA_values, gridSiteCode_rf, gridSiteCode_ro, save = TRUE) {

  ################################# 1 - Library loading & date format  ##########################################
  rwimes::logInfo("STEP 1: Loading libraries")

  library(airGR)
  library(rwimes)
  library(geojsonR)
  library(rgdal)
  library(raster)
  library(jsonlite)


  ################################# 2 - Reading parameters #######################################################
  rwimes::logInfo("STEP 2: Reading parameters")

  bv <- trimws(strsplit(bv_codes, ";")[[1]])
  ff <- trimws(strsplit(ff_codes, ";")[[1]])
  rf <- trimws(strsplit(rf_codes, ";")[[1]])
  fo <- trimws(strsplit(fo_codes, ";")[[1]])
  ro <- trimws(strsplit(ro_codes, ";")[[1]])
  S_GR4 <- trimws(strsplit(S_GR4_codes, ";")[[1]])
  R_GR4 <- trimws(strsplit(R_GR4_codes, ";")[[1]])
  AA <- as.numeric(trimws(strsplit(AA_values, ";")[[1]]))
  ECH <- as.numeric(trimws(strsplit(ECH_values, ";")[[1]]))
  BB <- as.numeric(trimws(strsplit(BB_values, ";")[[1]]))
  CC <- as.numeric(trimws(strsplit(CC_values, ";")[[1]]))
  area <- as.numeric(trimws(strsplit(AREA_values, ";")[[1]]))

  if (length(bv) != length(ff)) stop("bv_codes and ff_codes have not the same number of data")
  if (length(bv) != length(rf)) stop("bv_codes and rf_codes have not the same number of data")
  if (length(bv) != length(fo)) stop("bv_codes and fo_codes have not the same number of data")
  if (length(bv) != length(ro)) stop("bv_codes and ro_codes have not the same number of data")
  if (length(bv) != length(S_GR4)) stop("bv_codes and S_GR4_codes have not the same number of data")
  if (length(bv) != length(R_GR4)) stop("bv_codes and R_GR4_codes have not the same number of data")
  if (length(bv) != length(AA)) stop("bv_codes and AA_values have not the same number of data")
  if (length(bv) != length(ECH)) stop("bv_codes and ECH_values have not the same number of data")
  if (length(bv) != length(BB)) stop("bv_codes and BB_values have not the same number of data")
  if (length(bv) != length(CC)) stop("bv_codes and CC_values have not the same number of data")
  if (length(bv) != length(area)) stop("bv_codes and AREA_values have not the same number of data")
  
  watershed_number <- length(bv)
  progression_step_number <- watershed_number + 1

  ################################# 3 - Reading rainfall  ########################################################
  rwimes::logInfo("STEP 3: Reading rainfall forecast and observation")
  rwimes::updateProgression(round(1/3 / progression_step_number * 100), "Getting rainfall forecast and observetion")
  
  # Defines start forecast date (today at midnight)
  #startDate <- as.POSIXct(Sys.Date(), tz = "UTC")

  # Gets forecats rainfall as tiff
  forecastTiffs <- LoadForecastRainfallTiff(gridSiteCode_rf)
  observedTiff <- LoadObservedRainfallTiff(gridSiteCode_ro)

  ################################# 4 - loop on all watershed ####################################################
  rwimes::logInfo("STEP 4: Reading rainfall forecast and observation")
  # production date = Now

  date_prevision_Q <- Sys.time()
  for (i in seq(seq_along(bv))) {
    rwimes::updateProgression(round(i/progression_step_number * 100), paste("Calculation for", bv[i]))
    d_airgr_wimes_cambodia(bv[i], ff[i], rf[i], forecastTiffs, S_GR4[i], R_GR4[i], AA[i], ECH[i], BB[i], CC[i], area[i], observedTiff, fo[i], ro[i], date_prevision_Q, save)
  }
  
  
  for (tiff in forecastTiffs$tiff){
      print(tiff)
      file.remove(tiff)
  }
  file.remove(observedTiff$tiff)
}



#' Run the GR4J model for Liberia basins
#'
#' @param bvCode - code of the polygon site that represents the watersheed of the prevision point
#' @param ffCode - code of the timeseries which will recieve the flow forecast
#' @param rfCode - code of the timeseries which will recieve the precipitation forecast
#' @param forecastTiffs - data.frame of the rainfall forecats
#' @param S_GR4 - code of a intenal timeseries for real-time calculation
#' @param R_GR4 - code of a intenal timeseries for real-time calculation
#' @param AA - parameter of GR4J model x1
#' @param ECH - parameter of GR4J model x2
#' @param BB - parameter of GR4J model x3
#' @param CC - parameter of GR4J model x4
#' @param SUPERFICIE - basin area in square kilometers
#' @param observedTiff - data.frame of the observed rainfall as tiff
#' @param foCode - code of the timeseries which will recieve the observed flow
#' @param roCode - code of the timeseries which will recieve the observed precipitation
#' @param date_prevision_Q - date at which the forecast is done
#' @param save - boolean to indicate wheter results have to be saved
#'
d_airgr_wimes_cambodia <- function(bvCode, ffCode, rfCode, forecastTiffs, S_GR4, R_GR4, AA, ECH, BB, CC, SUPERFICIE, observedTiff, foCode, roCode, date_prevision_Q,  save = TRUE) {

  tryCatch({

    rwimes::logInfo(paste("-----------------------------------------------------------------------"))
    rwimes::logInfo(paste("Calculation for", bvCode))

    #################################### 2 - Basin rainfall calculating ############################################
    rwimes::logInfo("-- Calculating basin rainfall")

    # Returns the geometry of the site as a GDAL polygon
    rwimes::logInfo("---- Getting the geometry of the site as a GDAL polygon")
    bv <- GetGeometry(bvCode)

    # Calculates basin forecast raifall
    rwimes::logInfo("---- Calculating basin forecast raifall ")
    dailyRainfallForecast <- CalculateForecastRainfallBasinFromTiff(forecastTiffs, bv)

    # Calculates observed basin rainfall
    rwimes::logInfo("---- Calculating basin observed raifall")
    ro_bv_d <- CalculateObservedRainfallBasinFromTiff(observedTiff, bv)

    ##################################################### 3 - ETP ##################################################
    rwimes::logInfo("-- ETP")

    # Reads ETP paramters
    rwimes::logInfo("---- Reading ETP parameters")
    params <- ETPParameters()

    rwimes::logInfo("---- Assigning ETP parameters")
    ETP_MEAN_DAY_DATE <- params[[1]]
    ETP_MEAN_DAY <- params[[2]]

    # Generates ETP dates following rainfall dates
    DATE_ETP_JOUR <- paste0(substr(dailyRainfallForecast[, 1], 9, 10), "/", substr(dailyRainfallForecast[, 1], 6, 7))
    ETP_DATE <- rep(0, length(dailyRainfallForecast$Date))

    # Calculate ETP in forecast period
    for (i in seq_along(ETP_DATE)) {
      ETP_DATE[i] <- ETP_MEAN_DAY[which(ETP_MEAN_DAY_DATE == DATE_ETP_JOUR[i])]
    }

    ####################################### 4 - Flow forecast calculation ############################################
    rwimes::logInfo("-- Running GR4J")

    GR4JResult <- GR4JCalculation(dailyRainfallForecast, S_GR4, R_GR4, ETP_DATE, AA, ECH, BB, CC, SUPERFICIE)
    DEBIT_SIM <- GR4JResult[[1]]
    S_OUT <- GR4JResult[[2]]
    R_OUT <- GR4JResult[[3]]

    ####################################### 5 - Other calculation ###################################################
    rwimes::logInfo("-- Computing observed flow")
    # as no observed flow data, we replace by flow forecast d-1
    ffo <- tryCatch({
      tmp <- rwimes::getForecastValues(ffCode, as.POSIXct(dailyRainfallForecast[1, 1] - 1))$value
      if (is.null(tmp)) {
        rwimes::logWarning("No value for yesterday flow forecast")
        NULL
      } else {
        utils::tail(tmp, 1)
      }
    },
        error = function(e) {
          rwimes::logWarning(e$message)
          return(NULL)
        }
    )

    ############################################ 6 - Results writting  ###############################################
    rwimes::logInfo("-- Writting results")

    # Prepares and writes forecast data

    flow_forecast_dataframe <-data.frame(
                                    timeSeriesCode = replicate(length(dailyRainfallForecast$Date), ffCode),
                                    productionDateTime = as.POSIXct(replicate(length(dailyRainfallForecast$Date), date_prevision_Q), origin="1970-01-01"),
                                    forecastDateTime = as.POSIXct(dailyRainfallForecast$Date, origin = "1970-01-01"),
                                    value = DEBIT_SIM)

    rainf_forecast_dataframe <- data.frame(
                                    timeSeriesCode = replicate(length(dailyRainfallForecast$Date), rfCode),
                                    productionDateTime = as.POSIXct(replicate(length(dailyRainfallForecast$Date), date_prevision_Q), origin = "1970-01-01"),
                                    forecastDateTime = as.POSIXct(dailyRainfallForecast$Date + 1, origin = "1970-01-01"),
                                    value = dailyRainfallForecast$Rainfall)

    forecast_dataframe <- merge(flow_forecast_dataframe, rainf_forecast_dataframe, all = TRUE)

    if (!save) {
      print(forecast_dataframe)
    } else {
      rwimes::upsertQuantForecastValues(forecast_dataframe)
    }

    # Prepares and writes observed data

    sgr4_datframe <- data.frame(timeSeriesCode = replicate(length(dailyRainfallForecast$Date), S_GR4),
                               observationDateTime = as.POSIXct(dailyRainfallForecast$Date, origin = "1970-01-01"),
                               value = S_OUT[, 2])

    rgr4_datframe <- data.frame(timeSeriesCode = replicate(length(dailyRainfallForecast$Date), R_GR4),
                               observationDateTime = as.POSIXct(dailyRainfallForecast$Date, origin = "1970-01-01"),
                               value = R_OUT[, 2])

    o_datframe <- data.frame(timeSeriesCode = c(foCode, roCode),
                            observationDateTime = as.POSIXct(c(dailyRainfallForecast$Date[1] - 1, dailyRainfallForecast$Date[1]), origin = "1970-01-01"),
                            value = c(ffo, ro_bv_d[2]))

    observed_dataframe <- merge(merge(sgr4_datframe, rgr4_datframe, all = TRUE), o_datframe, all = TRUE)

    if (!save) {
      print(observed_dataframe)
    } else {
      rwimes::upsertQuantValues(observed_dataframe)
    }

     
    ############################################ END  ###############################################
    rwimes::logInfo("-- END")

  },

    error = function(e) {
      rwimes::logError(e$message)
    }
  )
}